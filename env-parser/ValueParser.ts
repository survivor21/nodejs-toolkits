import { Type } from "./EnvParser";
import { BooleanType, NumberType, StringType } from "./ValueTypes";

export default class ValueParser {
    public static parse(value: any, type: Type) {
        switch (type) {
            case Type.NUMBER:
                return NumberType.parse(value);
            case Type.STRING:
                return StringType.parse(value);
            case Type.BOOLEAN:
                return BooleanType.parse(value);
        }
    }
}
